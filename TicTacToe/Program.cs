﻿using System;

namespace TicTacToe
{
    /// <summary>
    /// Represents console Tic-Tac-Toe game.
    /// </summary>
    partial class Program
    {
        public const char X = 'X';
        public const char O = 'O';
        public const char EMPTY = ' ';
        private const string WELCOME = "Welcome to the Tic-Tac-Toe game!\n\n\n";

        static string console = string.Empty;
        static int width = -1;
        static bool secondPlayerIsRealPerson;
        static bool firstPlayerMovesFirst;

        static readonly Player firstPlayer = new Player { Color = ConsoleColor.Red };
        static readonly Player secondPlayer = new Player { Color = ConsoleColor.Blue };
        static Player currentPlayer;
        static Grid grid;
        static int currentRow;
        static int currentColumn;
        static bool isEmpty;
        static bool gameContinues;
        static bool calculated;
        static Cell calculatedTargetCell;

        Program()
        {
        }

        static void Main(string[] args)
        {
            console += WELCOME;
            while (true)
            {
                ReadWidth();
                ReadSecondPlayer();
                ReadWhoMovesFirst();
                ReadTypeOfMark();

                WriteNew("\nPress any key to start game:");
                Console.ReadKey();

                console = string.Empty;
                InitGame();

                do
                {
                    Move();
                }
                while (gameContinues);
            }
        }

        private static void InitGame()
        {
            grid = new Grid(width);
            firstPlayer.Opponent = secondPlayer;
            secondPlayer.Opponent = firstPlayer;
            currentColumn = currentRow = 0;
        }

        private static void Move()
        {
            if (grid.EmptyCellsCount == 0 || !currentPlayer.CanWin(grid) ||
                (!grid.HasPossibleMovesFor(firstPlayer) && !grid.HasPossibleMovesFor(secondPlayer) && firstPlayer.Score == secondPlayer.Score))
            {
                currentColumn = currentRow = -1;
                ShowGrid();
                ShowStopMessage();
            }
            else
            {
                ShowGrid();
                ShowContinueMessage();
            }
        }
    }
}
