﻿#pragma warning disable SA1201

namespace TicTacToe
{
    /// <content>
    /// Contains methods for getting sequences count.
    /// </content>
    public partial class Grid
    {
        public int GetSequencesCountFor(Player player)
        {
            int count = 0;
            for (int row = 0; row < Width; row++)
            {
                for (int column = 0; column < Width; column++)
                {
                    if (grid[row, column].Player == player)
                    {
                        grid[row, column].Sequence = false;
                    }
                }
            }

            count += this.GetHorizontalSequencesCountFor(player);
            count += this.GetVerticalSequencesCountFor(player);
            count += this.GetDiagonalSequencesCountFor(player);
            return count;
        }

        private int GetHorizontalSequencesCountFor(Player player)
        {
            int count = 0;
            for (int row = 0; row < Width; row++)
            {
                for (int column = 0; column < Width - 2; column++)
                {
                    if (this.IsSequence(grid[row, column], grid[row, column + 1], grid[row, column + 2], player))
                    {
                        count++;
                        column += 2;
                    }
                }
            }

            return count;
        }

        private int GetVerticalSequencesCountFor(Player player)
        {
            int count = 0;
            for (int column = 0; column < Width; column++)
            {
                for (int row = 0; row < Width - 2; row++)
                {
                    if (this.IsSequence(grid[row, column], grid[row + 1, column], grid[row + 2, column], player))
                    {
                        count++;
                        row += 2;
                    }
                }
            }

            return count;
        }

        private int GetDiagonalSequencesCountFor(Player player)
        {
            int count = 0;
            for (int rowncolumn = 0; rowncolumn <= 2 * (Width - 1); rowncolumn++)
            {
                int i = rowncolumn < Width ? 0 : rowncolumn + 1 - Width;
                int j = rowncolumn < Width ? Width - rowncolumn - 1 : 0;

                for (int row = i, column = j; row < Width - 2 && column < Width - 2;)
                {
                    if (this.IsSequence(grid[row, column], grid[row + 1, column + 1], grid[row + 2, column + 2], player))
                    {
                        count++;
                        row += 2;
                        column += 2;
                    }

                    row++;
                    column++;
                }

                i = rowncolumn < Width ? rowncolumn : Width - 1;
                j = rowncolumn < Width ? 0 : rowncolumn + 1 - Width;

                for (int row = i, column = j; row >= 2 && column < Width - 2;)
                {
                    if (this.IsSequence(grid[row, column], grid[row - 1, column + 1], grid[row - 2, column + 2], player))
                    {
                        count++;
                        row -= 2;
                        column += 2;
                    }

                    row--;
                    column++;
                }
            }

            return count;
        }

        private bool IsSequence(Cell cell1, Cell cell2, Cell cell3, Player player)
        {
            bool result = cell1.Player == player && cell2.Player == player && cell3.Player == player;
            if (result)
            {
                cell1.Sequence = cell2.Sequence = cell3.Sequence = true;
            }

            return result;
        }
    }
}
