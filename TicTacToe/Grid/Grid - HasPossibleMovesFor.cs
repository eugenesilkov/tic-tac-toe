﻿#pragma warning disable SA1201

namespace TicTacToe
{
    /// <content>
    /// Contains methods for checking if player has possible moves.
    /// </content>
    public partial class Grid
    {
        public bool HasPossibleMovesFor(Player player)
        {
            return
                this.HasPossibleHorizontalMovesFor(player) ||
                this.HasPossibleVerticalMovesFor(player) ||
                this.HasPossibleDiagonalMovesFor(player);
        }

        private bool HasPossibleHorizontalMovesFor(Player player)
        {
            for (int row = 0; row < Width; row++)
            {
                for (int column = 0; column < Width - 2; column++)
                {
                    if (this.IsPossible(grid[row, column], grid[row, column + 1], grid[row, column + 2], player))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool HasPossibleVerticalMovesFor(Player player)
        {
            for (int column = 0; column < Width; column++)
            {
                for (int row = 0; row < Width - 2; row++)
                {
                    if (this.IsPossible(grid[row, column], grid[row + 1, column], grid[row + 2, column], player))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool HasPossibleDiagonalMovesFor(Player player)
        {
            for (int rowncolumn = 0; rowncolumn <= 2 * (Width - 1); rowncolumn++)
            {
                int i = rowncolumn < Width ? 0 : rowncolumn + 1 - Width;
                int j = rowncolumn < Width ? Width - rowncolumn - 1 : 0;

                for (int row = i, column = j; row < Width - 2 && column < Width - 2;)
                {
                    if (this.IsPossible(grid[row, column], grid[row + 1, column + 1], grid[row + 2, column + 2], player))
                    {
                        return true;
                    }

                    row++;
                    column++;
                }

                i = rowncolumn < Width ? rowncolumn : Width - 1;
                j = rowncolumn < Width ? 0 : rowncolumn + 1 - Width;

                for (int row = i, column = j; row >= 2 && column < Width - 2;)
                {
                    if (this.IsPossible(grid[row, column], grid[row - 1, column + 1], grid[row - 2, column + 2], player))
                    {
                        return true;
                    }

                    row--;
                    column++;
                }
            }

            return false;
        }

        private bool IsPossible(Cell cell1, Cell cell2, Cell cell3, Player player)
        {
            bool empty =
                cell1.Player == null ||
                cell2.Player == null ||
                cell3.Player == null;
            bool opponentOccupied =
                cell1.Player == player.Opponent ||
                cell2.Player == player.Opponent ||
                cell3.Player == player.Opponent;

            return empty && !opponentOccupied;
        }
    }
}
