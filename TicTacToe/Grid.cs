﻿using System.Collections.Generic;

#pragma warning disable SA1201

namespace TicTacToe
{
    /// <summary>
    /// Represents grid with cells.
    /// </summary>
    public partial class Grid
    {
        private Cell[,] grid;

        public Grid(int width)
        {
            this.Width = width;
            this.grid = new Cell[width, width];
            for (int row = 0; row < width; row++)
            {
                for (int column = 0; column < width; column++)
                {
                    this.grid[row, column] = new Cell(row, column);
                }
            }
        }

        public int Width { get; } = 0;

        public Grid Copy
        {
            get
            {
                Grid copy = new Grid(this.Width);
                for (int row = 0; row < this.Width; row++)
                {
                    for (int column = 0; column < this.Width; column++)
                    {
                        copy[row, column].Player = this[row, column].Player;
                        copy[row, column].Sequence = this[row, column].Sequence;
                    }
                }

                return copy;
            }
        }

        public Cell[] EmptyCells
        {
            get
            {
                Stack<Cell> stack = new Stack<Cell>();
                for (int row = 0; row < this.Width; row++)
                {
                    for (int column = 0; column < this.Width; column++)
                    {
                        if (this.grid[row, column].Player == null)
                        {
                            stack.Push(this.grid[row, column]);
                        }
                    }
                }

                return stack.ToArray();
            }
        }

        public int EmptyCellsCount
        {
            get
            {
                int count = 0;
                for (int row = 0; row < this.Width; row++)
                {
                    for (int column = 0; column < this.Width; column++)
                    {
                        if (this.grid[row, column].Player == null)
                        {
                            count++;
                        }
                    }
                }

                return count;
            }
        }

        public Cell this[int row, int column] => this.grid[row, column];

        public Grid GetFilled(Player player)
        {
            Grid copy = this.Copy;
            for (int row = 0; row < copy.Width; row++)
            {
                for (int column = 0; column < copy.Width; column++)
                {
                    if (copy[row, column].Player == null)
                    {
                        copy[row, column].Player = player;
                    }
                }
            }

            return copy;
        }
    }
}
