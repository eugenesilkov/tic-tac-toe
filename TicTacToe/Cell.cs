﻿using System;

namespace TicTacToe
{
    public class Cell
    {
        public Cell(int row, int column)
        {
            this.Row = row;
            this.Column = column;
        }

        public int Row { get; }

        public int Column { get; }

        public Player Player { get; set; }

        public bool Sequence { get; set; }

        public char GetMark(bool current, Player currentPlayer)
        {
            if (current)
            {
                if (this.Player == null)
                {
                    return currentPlayer.Mark;
                }
                else
                {
                    return '#';
                }
            }
            else
            {
                if (this.Player == null)
                {
                    return Program.EMPTY;
                }
                else
                {
                    return this.Player.Mark;
                }
            }
        }

        public ConsoleColor GetBackground(bool current)
        {
            if (current)
            {
                if (this.Player == null)
                {
                    return ConsoleColor.Green;
                }
                else
                {
                    return ConsoleColor.Yellow;
                }
            }
            else
            {
                if (this.Sequence)
                {
                    return this.Player.Color;
                }
                else
                {
                    return ConsoleColor.White;
                }
            }
        }

        public ConsoleColor GetForeground(bool current)
        {
            if (current)
            {
                return ConsoleColor.Black;
            }
            else
            {
                if (this.Sequence || this.Player == null)
                {
                    return ConsoleColor.White;
                }
                else
                {
                    return this.Player.Color;
                }
            }
        }
    }
}
