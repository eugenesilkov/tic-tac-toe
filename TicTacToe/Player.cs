﻿using System;

namespace TicTacToe
{
    public class Player
    {
        public string Name { get; set; }

        public char Mark { get; set; }

        public ConsoleColor Color { get; set; }

        public Player Opponent { get; set; }

        public int Score { get; private set; }

        public void RefreshScore(Grid grid)
        {
            this.Score = grid.GetSequencesCountFor(this);
        }

        public bool CanWin(Grid grid)
        {
            return grid.GetFilled(this).GetSequencesCountFor(this) >= this.Opponent.Score;
        }
    }
}
