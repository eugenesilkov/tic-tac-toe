﻿using System;

namespace TicTacToe
{
    /// <content>
    /// Contains methods for displaying scores.
    /// </content>
    partial class Program
    {
        private static void GetScores()
        {
            Console.WriteLine($"\nSCORES:");
            GetScoresOfPlayer(firstPlayer);
            GetScoresOfPlayer(secondPlayer);
            Console.WriteLine();
        }

        private static void GetScoresOfPlayer(Player player)
        {
            Console.ForegroundColor = player.Color;
            Console.Write(player.Name);
            Console.ResetColor();
            Console.WriteLine($" has earned {player.Score} points.");
        }
    }
}
