﻿using System;

#pragma warning disable SA1201

namespace TicTacToe
{
    /// <content>
    /// Contains keys methods.
    /// </content>
    partial class Program
    {
        private static void ReadGameControls()
        {
            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.UpArrow:
                    if (currentRow > 0)
                    {
                        currentRow--;
                    }

                    break;

                case ConsoleKey.DownArrow:
                    if (currentRow < width - 1)
                    {
                        currentRow++;
                    }

                    break;

                case ConsoleKey.LeftArrow:
                    if (currentColumn > 0)
                    {
                        currentColumn--;
                    }

                    break;

                case ConsoleKey.RightArrow:
                    if (currentColumn < width - 1)
                    {
                        currentColumn++;
                    }

                    break;

                case ConsoleKey.Enter:
                    gameContinues = false;
                    break;

                case ConsoleKey.Spacebar:
                    MakeChoice();
                    break;

                case ConsoleKey.Escape:
                    Environment.Exit(0);
                    break;
            }
        }

        private static void ReadResultControls()
        {
            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.Enter:
                    gameContinues = false;
                    Console.Clear();
                    break;

                case ConsoleKey.Escape:
                    Environment.Exit(0);
                    break;
            }
        }

        private static void MakeChoice()
        {
            if (isEmpty)
            {
                grid[currentRow, currentColumn].Player = currentPlayer;
                currentPlayer.RefreshScore(grid);
                currentPlayer = currentPlayer.Opponent;
                calculated = false;
                FindFirstEmptyCell();
            }
        }

        private static void FindFirstEmptyCell()
        {
            for (int row = 0; row < width; row++)
            {
                for (int column = 0; column < width; column++)
                {
                    if (grid[row, column].Player == null)
                    {
                        currentRow = row;
                        currentColumn = column;
                        return;
                    }
                }
            }
        }
    }
}
