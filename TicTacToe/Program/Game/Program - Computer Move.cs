﻿using System;
using System.Threading;

#pragma warning disable SA1201

namespace TicTacToe
{
    /// <content>
    /// Contains methods for computer move calculation.
    /// </content>
    partial class Program
    {
        private static void ActivateComputerMove()
        {
            if (!calculated)
            {
                Calculate();
            }

            Thread.Sleep(TimeSpan.FromMilliseconds(500));
            CheckCoordinates();
        }

        private static void Calculate()
        {
            calculated = true;
            Cell[] emptyCells = grid.EmptyCells;
            int value = 0;
            for (int i = 0; i < emptyCells.Length; i++)
            {
                int v = 0;
                Grid copy = grid.Copy;
                Cell emty = emptyCells[i];
                Player player;
                copy[emty.Row, emty.Column].Player = player = currentPlayer;
                v += copy.GetSequencesCountFor(player) - player.Score;
                copy[emty.Row, emty.Column].Player = player = currentPlayer.Opponent;
                v += copy.GetSequencesCountFor(player) - player.Score;

                if (value < v)
                {
                    value = v;
                    calculatedTargetCell = emty;
                }

                if (value == 0)
                {
                    calculatedTargetCell = emptyCells[new Random().Next(0, emptyCells.Length)];
                }
            }
        }

        private static void CheckCoordinates()
        {
            if (currentRow == calculatedTargetCell.Row && currentColumn == calculatedTargetCell.Column)
            {
                MakeChoice();
                return;
            }

            if (currentColumn < calculatedTargetCell.Column)
            {
                currentColumn++;
            }
            else if (currentColumn > calculatedTargetCell.Column)
            {
                currentColumn--;
            }

            if (currentRow < calculatedTargetCell.Row)
            {
                currentRow++;
            }
            else if (currentRow > calculatedTargetCell.Row)
            {
                currentRow--;
            }
        }
    }
}
