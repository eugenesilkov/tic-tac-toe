﻿using System;

#pragma warning disable SA1201

namespace TicTacToe
{
    /// <content>
    /// Contains displaying methods.
    /// </content>
    partial class Program
    {
        private static void ShowGrid()
        {
            Console.Clear();
            Console.Write("\n\n");
            for (int row = 0; row < width; row++)
            {
                Console.Write(new string(' ', 3));
                for (int column = 0; column < width; column++)
                {
                    WriteCell(row, column);
                }

                Console.Write("\n");
            }

            Console.Write("\n\n");
        }

        private static void ShowContinueMessage()
        {
            Console.WriteLine($"Current cell: {currentRow + 1} : {currentColumn + 1}");
            GetScores();
            WritePlayerTurn();
            Console.WriteLine("Use arrows to move cells in grid.\nPress 'Space' to place X/O.\nPress 'Enter' to restart.\nPress 'Esc' to quit.");
            gameContinues = true;
            if (currentPlayer == secondPlayer && !secondPlayerIsRealPerson)
            {
                ActivateComputerMove();
            }
            else
            {
                WriteErrorMessage();
                ReadGameControls();
            }
        }

        private static void ShowStopMessage()
        {
            GetScores();
            if (firstPlayer.Score == secondPlayer.Score)
            {
                Console.WriteLine("DRAW! There is no winner.");
            }
            else
            {
                GetWinner();
            }

            Console.WriteLine("Press 'Enter' to start a new game or 'Esc' to quit.");
            ReadResultControls();
        }

        private static void GetWinner()
        {
            Player winner = firstPlayer.Score > secondPlayer.Score ? firstPlayer : secondPlayer;
            Console.Write("The winner is ");
            Console.ForegroundColor = winner.Color;
            Console.Write(winner.Name);
            Console.ResetColor();
            Console.WriteLine($" with score: {winner.Score}.");
        }

        private static void WriteErrorMessage()
        {
            if (!isEmpty)
            {
                Console.BackgroundColor = ConsoleColor.Yellow;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.Write("CAUTION:");
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write(" This cell is now occupied. Please, choose another one.");
                Console.ResetColor();
            }

            Console.WriteLine();
        }

        private static void WritePlayerTurn()
        {
            Console.Write("Now is ");
            Console.ForegroundColor = currentPlayer.Color;
            Console.Write(currentPlayer.Name);
            Console.ResetColor();
            Console.Write("' turn with ");
            Console.ForegroundColor = currentPlayer.Color;
            Console.Write(currentPlayer.Mark);
            Console.ResetColor();
            Console.WriteLine(".");
        }

        private static void WriteCell(int row, int column)
        {
            bool current = row == currentRow && column == currentColumn;
            Cell cell = grid[row, column];
            Console.BackgroundColor = cell.GetBackground(current);
            Console.ForegroundColor = cell.GetForeground(current);
            Console.Write(cell.GetMark(current, currentPlayer));
            Console.ResetColor();
            if (current)
            {
                isEmpty = grid[row, column].Player == null;
            }
        }
    }
}
