﻿using System;

namespace TicTacToe
{
    /// <content>
    /// Contains additional output methods.
    /// </content>
    partial class Program
    {
        private static void Write(string str)
        {
            Console.Clear();
            Console.Write(console);
            Console.WriteLine(str);
        }

        private static void WriteNew(string str)
        {
            Console.Clear();
            console += $"{str}\n";
            Console.Write(console);
        }
    }
}
