﻿using System;

namespace TicTacToe
{
    /// <content>
    /// Contains input methods.
    /// </content>
    partial class Program
    {
        private static void ReadWidth()
        {
            bool handled = false;
            string excMessage = string.Empty;
            do
            {
                try
                {
                    Write("\n" + excMessage);

                    string enterMessage = "Enter grid width: ";
                    string input = Read(enterMessage);

                    HandleExceptionWithGridWidth(input, out width);
                    WriteNew("\n\n" + enterMessage + input);

                    handled = true;
                }
                catch (Exception exc)
                {
                    excMessage = exc.Message;
                    Console.WriteLine($"{excMessage}\n");
                }
            }
            while (!handled);
        }

        private static void ReadSecondPlayer()
        {
            bool handled = false;
            string excMessage = string.Empty;
            do
            {
                try
                {
                    Write("\n" + excMessage);

                    string enterMessage = "Would you like to play with a real person (Y) or a computer (N) ?: ";
                    ConsoleKey key = ReadKey(enterMessage);

                    HandleExceptionWithSecondPlayer(key);

                    secondPlayerIsRealPerson = key == ConsoleKey.Y;
                    if (secondPlayerIsRealPerson)
                    {
                        firstPlayer.Name = $"Player 1 ({firstPlayer.Color})";
                        secondPlayer.Name = $"Player 2 ({secondPlayer.Color})";
                    }
                    else
                    {
                        firstPlayer.Name = "Player";
                        secondPlayer.Name = "Computer";
                    }

                    string opponent = secondPlayerIsRealPerson ? "a real person" : "a computer";
                    WriteNew($"\n\n{enterMessage}{key}\nYou chose {opponent} to play with.");

                    handled = true;
                }
                catch (Exception exc)
                {
                    excMessage = exc.Message;
                    Console.WriteLine($"{excMessage}\n");
                }
            }
            while (!handled);
        }

        private static void ReadWhoMovesFirst()
        {
            bool handled = false;
            string excMessage = string.Empty;
            do
            {
                try
                {
                    Write("\n" + excMessage);

                    string enterMessage = "Would you like to move first (1) or second (2) or choose at random (3) ?: ";
                    ConsoleKey key = ReadKey(enterMessage);

                    HandleExceptionWithWhoMovesFirst(key);
                    if (key == ConsoleKey.D3 || key == ConsoleKey.NumPad3)
                    {
                        firstPlayerMovesFirst = new Random().Next(0, 2) == 1;
                    }
                    else
                    {
                        firstPlayerMovesFirst = key == ConsoleKey.D1 || key == ConsoleKey.NumPad1;
                    }

                    currentPlayer = firstPlayerMovesFirst ? firstPlayer : secondPlayer;
                    string who = firstPlayerMovesFirst ? "You move first." : "Your opponent moves first.";
                    WriteNew($"\n\n{enterMessage}{$"{key}"[^1..]}\n{who}");
                    Console.ResetColor();

                    handled = true;
                }
                catch (Exception exc)
                {
                    excMessage = exc.Message;
                    Console.WriteLine($"{excMessage}\n");
                }
            }
            while (!handled);
        }

        private static void ReadTypeOfMark()
        {
            bool handled = false;
            string excMessage = string.Empty;
            do
            {
                try
                {
                    Write("\n" + excMessage);

                    string enterMessage = "Would you like to place Crosses (X) or Noughts (O) ?: ";
                    ConsoleKey key = ReadKey(enterMessage);

                    HandleExceptionWithTypeOfMark(key);
                    firstPlayer.Mark = key == ConsoleKey.X ? X : O;
                    secondPlayer.Mark = key == ConsoleKey.X ? O : X;
                    WriteNew($"\n\n{enterMessage}{key}\nYour mark is {firstPlayer.Mark}.\nYour opponent's mark is {secondPlayer.Mark}.");

                    handled = true;
                }
                catch (Exception exc)
                {
                    excMessage = exc.Message;
                    Console.WriteLine($"{excMessage}\n");
                }
            }
            while (!handled);
        }

        private static string Read(string message)
        {
            Console.Write(message);
            return Console.ReadLine();
        }

        private static ConsoleKey ReadKey(string message)
        {
            Console.Write(message);
            return Console.ReadKey(false).Key;
        }
    }
}
