﻿using System;

#pragma warning disable S3928

namespace TicTacToe
{
    /// <content>
    /// Contains methods for handler.
    /// </content>
    partial class Program
    {
        private static void HandleExceptionWithGridWidth(string s, out int n)
        {
            if (string.IsNullOrWhiteSpace(s))
            {
                throw new ArgumentNullException(null, "You should enter a value.");
            }

            if (!int.TryParse(s, out n))
            {
                throw new ArgumentException("You should enter a number.");
            }

            if (n < 3)
            {
                throw new ArgumentException("You should enter a number greater than or equal to 3.");
            }

            if (n > 75)
            {
                throw new ArgumentException("The width value is too big. You should enter a number less than or equal to 75.");
            }

            if (n % 2 == 0)
            {
                throw new ArgumentException("You should enter an odd number.");
            }
        }

        private static void HandleExceptionWithSecondPlayer(ConsoleKey key)
        {
            if (key != ConsoleKey.Y && key != ConsoleKey.N)
            {
                throw new ArgumentException("You should press either 'Y' or 'N'.");
            }
        }

        private static void HandleExceptionWithWhoMovesFirst(ConsoleKey key)
        {
            if (key != ConsoleKey.D1 &&
                key != ConsoleKey.D2 &&
                key != ConsoleKey.D3 &&
                key != ConsoleKey.NumPad1 &&
                key != ConsoleKey.NumPad2 &&
                key != ConsoleKey.NumPad3)
            {
                throw new ArgumentException("You should press '1' or '2' or '3'.");
            }
        }

        private static void HandleExceptionWithTypeOfMark(ConsoleKey key)
        {
            if (key != ConsoleKey.X && key != ConsoleKey.O)
            {
                throw new ArgumentException("You should press either 'X' or 'O'.");
            }
        }
    }
}
